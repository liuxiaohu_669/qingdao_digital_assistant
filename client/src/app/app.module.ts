import { CardDetailsPageModule } from "./../pages/card-details/card-details.module";
import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";
import { StatusBar } from "@ionic-native/status-bar";
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { SplashScreen } from "@ionic-native/splash-screen";
import { LoginPage } from "../pages/login/login";
import { HomePage } from "../pages/home/home";
import { CustomerPage } from "../pages/customer/customer";
import { BusinessScenarioPage } from "../pages/business-scenario/business-scenario";
import { ComponentsModule } from "../components/components.module";
import { CustomerManagerRecommendationPageModule } from "../pages/customer-manager-recommendation/customer-manager-recommendation.module";
import { CustomerPageModule } from "../pages/customer/customer.module";
import { CustomerNewPageModule } from "../pages/customer-new/customer-new.module";
import { IonicStorageModule } from "@ionic/storage";
import { BusinessProcessPageModule } from "../pages/business-process/business-process.module";
import { BusinessScenarioPageModule } from "../pages/business-scenario/business-scenario.module";
import { HttpModule } from "@angular/http";
import { CaseShowPageModule } from "../pages/case-show/case-show.module";
import { CaseDetailsPageModule } from "../pages/case-details/case-details.module";
import { CheckListPageModule } from "../pages/check-list/check-list.module";
import { DemandTrackingPageModule } from "../pages/demand-tracking/demand-tracking.module";
import { CostEstimatePageModule } from "../pages/cost-estimate/cost-estimate.module";
import { PlanResultsPageModule } from "../pages/plan-results/plan-results.module";
import { FormsModule } from "@angular/forms";
import { EmitService } from "../services/service";
import { UserInfoService } from "../services/user";
import { NgxQRCodeModule } from 'ngx-qrcode2';
@NgModule({
  declarations: [MyApp, LoginPage, HomePage],
  imports: [
    FormsModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    IonicStorageModule.forRoot(),
    CustomerManagerRecommendationPageModule,
    CustomerPageModule,
    CustomerNewPageModule,
    BusinessProcessPageModule,
    BusinessScenarioPageModule,
    CardDetailsPageModule,
    CaseDetailsPageModule,
    CaseShowPageModule,
    CheckListPageModule,
    DemandTrackingPageModule,
    CostEstimatePageModule,
    PlanResultsPageModule,
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    CustomerPage,
    BusinessScenarioPage
  ],
  providers: [
    StatusBar,
    ScreenOrientation,
    SplashScreen,
    EmitService,
    UserInfoService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
