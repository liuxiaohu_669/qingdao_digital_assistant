import { Component } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Platform } from "ionic-angular";
import { LoginPage } from "../pages/login/login";

@Component({ templateUrl: "app.html" })
export class MyApp {
  rootPage = LoginPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    screenOrientation:ScreenOrientation
  ) {
    platform.ready().then(() => {
      if(platform.is('core') || platform.is('mobileweb')){
        // web端
      }else{
        screenOrientation.lock(screenOrientation.ORIENTATIONS.LANDSCAPE);
        statusBar.overlaysWebView(false);
        statusBar.hide();
        splashScreen.hide();
      }
    });
  }
}
