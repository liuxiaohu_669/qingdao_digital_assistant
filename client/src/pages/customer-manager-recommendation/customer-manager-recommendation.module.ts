import { NgModule } from "@angular/core";
import { IonicPageModule, IonicModule } from "ionic-angular";
import { CustomerManagerRecommendationPage } from "./customer-manager-recommendation";
import { ComponentsModule } from "../../components/components.module";
import { CustomerManagerInfoPage } from "../customer-manager-info/customer-manager-info";

@NgModule({
  declarations: [CustomerManagerRecommendationPage, CustomerManagerInfoPage],
  imports: [
    IonicModule,
    IonicPageModule.forChild(CustomerManagerRecommendationPage),
    ComponentsModule
    // CustomerManagerInfoPageModule
  ]
})
export class CustomerManagerRecommendationPageModule {}
