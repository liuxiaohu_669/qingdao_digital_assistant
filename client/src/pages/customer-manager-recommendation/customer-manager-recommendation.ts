import { Component, ViewChild, Output, EventEmitter } from "@angular/core";
import { IonicPage, NavController, NavParams, Slides } from "ionic-angular";
import { Http } from "@angular/http";
import { EmitService } from "../../services/service";
@IonicPage()
@Component({
  selector: "page-customer-manager-recommendation",
  templateUrl: "customer-manager-recommendation.html"
})
export class CustomerManagerRecommendationPage {
  id: number = 0;
  @ViewChild(Slides) slides: Slides;
  change: EventEmitter<number>;
  isShowInfo = false;
  customerManagerList = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public emitService: EmitService
  ) {}
  ionViewDidLoad() {
    // 客户经理列表
    this.http
      .get(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzcustomemanager/list"
      )
      .subscribe(data => {
        var List = JSON.parse(data.text()).page.list;
        var length = Math.ceil(List.length / 3);
        var listlength = -1;
        for (let i = 0; i < length; i++) {
          var item = [];
          for (let p = 0; p < 3; p++) {
            if (listlength < List.length - 1) {
              listlength++;
              item.push(List[listlength]);
            }
          }
          this.customerManagerList.push(item);
        }
      });
  }

  hideInfoPage() {
    this.isShowInfo = false;
  }
  showInfoPage(id) {
    this.isShowInfo = true;
    setTimeout(() => {
      this.emitService.eventEmit.emit(id);
    },200)
  }
}
