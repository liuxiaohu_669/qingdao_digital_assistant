import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Component, ViewChild } from "@angular/core";
import { NavController, Nav } from "ionic-angular";
// import { CustomerPage } from "../customer/customer";
import { BusinessProcessPage } from "../business-process/business-process";
import { BusinessScenarioPage } from "../business-scenario/business-scenario";
import { CaseShowPage } from "../case-show/case-show";
import { CustomerManagerRecommendationPage } from "../customer-manager-recommendation/customer-manager-recommendation";
import { DemandTrackingPage } from "../demand-tracking/demand-tracking";
import { Http } from "@angular/http";
// @IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("content") nav: Nav;

  root = CustomerManagerRecommendationPage;
  tab1Image: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
    "assets/imgs/tab1.png"
  );
  tab2Image: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
    "assets/imgs/tab2.png"
  );
  tab3Image: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
    "assets/imgs/tab3.png"
  );
  tab4Image: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
    "assets/imgs/tab4.png"
  );
  tab5Image: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
    "assets/imgs/tab5.png"
  );

  selectTabIndex: number = 0;

  constructor(public navCtrl: NavController, public sanitizer: DomSanitizer, public http: Http) {
    // this.http.get('https://www.reddit.com/r/gifs/new/.json?limit=10').subscribe(data => {
    //     // this.posts = data.data.children;
    //     // alert(data);
    // });
  }

  togglePage(tabIndex: number): void {
    if (tabIndex === this.selectTabIndex) {
      return;
    }
    switch (tabIndex) {
      case 0:
        this.nav.setRoot(CustomerManagerRecommendationPage);
        break;
      case 1:
        this.nav.setRoot(DemandTrackingPage);
        break;
      case 2:
        this.nav.setRoot(BusinessProcessPage);
        break;
      case 3:
        this.nav.setRoot(BusinessScenarioPage);
        break;
      case 4:
        this.nav.setRoot(CaseShowPage);
        break;
    }
    this.selectTabIndex = tabIndex;
  }
}
