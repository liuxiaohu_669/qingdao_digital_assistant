import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CustomerNewPage } from "../customer-new/customer-new";

@Component({
  selector: "page-customer",
  templateUrl: "customer.html"
})
export class CustomerPage {
  customerList: any[];

  constructor(public navCtrl: NavController) {
    this.customerList = [
      {
        type: "a",
        member: [1, 2]
      },
      {
        type: "b",
        member: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
      }
    ];
  }

  onClickNewCustomerButton() {
    this.navCtrl.push(CustomerNewPage, {}, {animate: true})
  }
}
