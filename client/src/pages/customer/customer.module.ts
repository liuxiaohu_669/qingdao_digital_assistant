import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { CustomerPage } from './customer';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CustomerPage
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(CustomerPage),
    ComponentsModule
  ],
})
export class CustomerPageModule {}
