import { Component , ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlanResultsPage } from '../plan-results/plan-results';
import { Http } from "@angular/http";
import { UserInfoService } from "../../services/user";
/**
 * Generated class for the CostEstimatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cost-estimate',
  templateUrl: 'cost-estimate.html',
})
export class CostEstimatePage {

  pageData=this.navParams.data;
  SBID=this.pageData.SBid==="国产柜（合资断路器）"?2:1;
  FALXname="";
  JTPZname="";
  GCMS="";
  SYFW="";
  imgsrc="";
  SM="";//底部说明
  JKX={
    num:10,
    Shownum:"请输入架空线长度"
  };
  GD={
    num:10,
    Shownum:"请输入管道长度"
  };
  DL={
    num:10,
    Shownum:"请输入电缆长度"
  };
  TABLE=[[],[],[],[]];//设备数组
  TITLE1={};//10kv侧设备第一条
  isnull1=false;
  isnull2=false;
  TITLE2={};//0.4kv侧设备第一条
  SJF=0;//设计费
  SGF=0;//施工费
  SJFBFB=0;//设计费占用百分比
  SGFBFB=0;//施工费占用百分比
  HJ=0;//合计费用
  GCZZJ=0;//工程总造价
  SBZJG=0;//设备总造价
  HS=0;
  SBZJGY=0;
  SJC="";//方案生成时间戳
  FAYXQ=""//方案有效期
  TP=false;
  TB=false;
  constructor(public navCtrl: NavController, public navParams: NavParams , public http:Http,public user:UserInfoService) {
  }

  ionViewDidLoad() {
    //查询方案类型
    this.http.get(
      "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzschemetype/info/"+this.pageData.FALXid
    ).subscribe(data=>{
      var FALX=JSON.parse(data.text()).zzSchemeType;
      this.FALXname=FALX.typeName;
      for(let i=0;i<FALX.zzSchemeTypeConfigurationEntityList.length;i++){
        if(FALX.zzSchemeTypeConfigurationEntityList[i].id===this.pageData.JTPZid){
          this.JTPZname=FALX.zzSchemeTypeConfigurationEntityList[i].configurationName;
          this.GCMS=FALX.zzSchemeTypeConfigurationEntityList[i].cRemark;
          this.SYFW=FALX.zzSchemeTypeConfigurationEntityList[i].applicationScope;
          this.SM=FALX.zzSchemeTypeConfigurationEntityList[i].instructions;
          if(FALX.zzSchemeTypeConfigurationEntityList[i].imgPath!=null){
            this.TP=true;
          }else{
            this.TP=false;
          }
          this.imgsrc="http://liuxh1819.vicp.cc/renren-fast"+FALX.zzSchemeTypeConfigurationEntityList[i].imgPath;

          //方案具体设施
          this.http.get(
            "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzelectricalequipment/info/"+this.pageData.JTPZid
          ).subscribe(data=>{
            var Data=JSON.parse(data.text()).zzElectricalEquipment;
            if(Data.length==0){
              this.TB=false;
            }else{
              this.TB=true;
            }
            for(let i=0;i<Data.length;i++){
              console.log(this.SBID);
              this.SBZJGY+=(Data[i].deviceTotal*(this.SBID==1?Data[i].domesticArk:Data[i].jointVenture));
              this.HS+=1;
              switch (Data[i].typeId){
                case "1":
                  if(typeof this.TITLE1.typeId==="undefined"){
                    this.TITLE1=Data[i];
                    this.isnull1=true;
                  }else{
                    this.TABLE[0].push(Data[i]);
                  }
                  break;
                case "2":
                  if(typeof this.TITLE2.typeId==="undefined"){
                    this.TITLE2=Data[i];
                    this.isnull2=true;
                  }else{
                    this.TABLE[1].push(Data[i]);
                  }
                  break;
                case "3":
                  this.TABLE[2].push(Data[i]);
                  break;
                case "4":
                  this.TABLE[3].push(Data[i]);
                  break;
              }
            }
            this.HS+=2;
            this.SGFBFB=parseFloat(this.SM.substring(this.SM.indexOf("%")-2,this.SM.indexOf("%")))/100;
            this.SJFBFB=parseFloat(this.SM.substring(this.SM.lastIndexOf("%")-3,this.SM.lastIndexOf("%")))/100;
            this.Calculation();
          });
        }
      }
    });


    //查询线路管道架空线等
    this.http
      .get(
        "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzmodelassort/list"
      )
      .subscribe(data => {
        var Data = JSON.parse(data.text()).page.list;
        for(let i=0;i<Data.length;i++){
          if(Data[i].id===this.pageData.JKXid){
            this.JKX.name=Data[i].modelName;
            this.JKX.jiage=Data[i].price;
            this.JKX.Shownum=this.JKX.jiage*this.JKX.num+"元";
          }
          if(Data[i].id===this.pageData.DLid){
            this.DL.name=Data[i].modelName;
            this.DL.jiage=Data[i].price;
            this.DL.Shownum=this.DL.jiage*this.DL.num+"元";
          }
          if(Data[i].id===this.pageData.GDid){
            this.GD.name=Data[i].modelName;
            this.GD.jiage=Data[i].price;
            this.GD.Shownum=this.GD.jiage*this.GD.num+"元";
          }
        }
      });
  }

  Calculation(){
    this.SBZJG=this.SBZJGY;
    this.SGF=0;
    this.SJF=0;

    console.log(this.SBZJG);
    this.SGF+=this.SBZJG*this.SGFBFB;

    this.SJF=Math.round(this.SBZJG*this.SJFBFB);




    this.HJ=this.SBZJG+this.SGF+this.SJF;

    this.GCZZJ=0;

    this.GCZZJ+=this.JKX.jiage*this.JKX.num;
    this.GCZZJ+=this.GD.jiage*this.GD.num;
    this.GCZZJ+=this.DL.jiage*this.DL.num;

    this.GCZZJ+=this.HJ;

  }

  BianJi(){
    var jd=document.getElementById("Jiakx");
    jd.focus();
  }

  QueDing(){
    this.JKX.Shownum=(this.JKX.num*this.JKX.jiage)+"元";
    this.DL.Shownum=(this.DL.num*this.DL.jiage)+"元";
    this.GD.Shownum=(this.GD.num*this.GD.jiage)+"元";
    this.Calculation();
  }

  ClickLink(){
    this.FAYXQ="";
    this.SJC="DP"+Date.parse(new Date());

    var date=new Date(new Date().setDate(new Date().getDate() + 60));
    this.FAYXQ+=date.getFullYear()+"-";
    this.FAYXQ+=date.getMonth()+"-";
    this.FAYXQ+=date.getDate();
    this.http.post(
      "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzgeneratescheme/save",
      {
        "applyScope": this.SYFW,
        "cableModel": this.DL.name,
        "cablePrice": this.DL.jiage,
        "cableNumber":this.DL.num,
        "configurationName": this.JTPZname,
        "designerPeople": this.user.getUser().name,
        "overheadLineModel": this.JKX.name,
        "overheadLinesPrice": this.JKX.jiage,
        "overheadLinesNumber":this.JKX.num,
        "pipePrice": this.GD.jiage,
        "pipelineModel": this.GD.name,
        "pipeNumber":this.GD.num,
        "planLabel": this.FALXname+"、"+this.JTPZname,
        "planPicture": this.imgsrc,
        "planReckon": this.GCZZJ,
        "projectDescription": this.GCMS,
        "totalProjectPrice": this.GCZZJ,
        "typeName": this.FALXname,
        "planNumber":this.SJC,
        "validData":this.FAYXQ,
        "domestic":this.pageData.SBid,
        "instructions":this.SM
      }
    ).subscribe(data=>{
      var Data=JSON.parse(data.text());
      this.navCtrl.push(PlanResultsPage, {id:this.SJC,xid:this.pageData.JTPZid}, {animate: true})
    })
    // this.navCtrl.push(PlanResultsPage, {}, {animate: true})
  }

}
