import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CostEstimatePage } from './cost-estimate';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    CostEstimatePage,
  ],
  imports: [
    IonicPageModule.forChild(CostEstimatePage),
    ComponentsModule
  ],
})
export class CostEstimatePageModule {}
