import {
  Component,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Chart } from "chart.js";
import { Http } from "@angular/http";
import { EmitService } from "../../services/service";
import * as scharts from 'echarts';
// @IonicPage()
@Component({
  selector: "page-customer-manager-info",
  templateUrl: "customer-manager-info.html"
})
export class CustomerManagerInfoPage {
  radar: ElementRef;
  bar: ElementRef;
  isShow = false;
  @ViewChild("radar")
  @Output()
  changeNumber: EventEmitter<number> = new EventEmitter();
  set radarContent(content: ElementRef) {
    this.radar = content;
    this.initRadarChart();
  }

  @ViewChild("bar")
  set barContent(content: ElementRef) {
    this.bar = content;
    this.initBarChart();
  }

  @Output() close = new EventEmitter();
  @Input() content: string;
  items={};
  itemsshow=[];
  JiaoYu=[];
  RongYu=[];
  ZhuanYe=[];
  PJDS=0;
  url="http://liuxh1819.vicp.cc/renren-fast/";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public emitService: EmitService
  ) {
  }

  ngOnInit() {
    var a=this.emitService.eventEmit.subscribe((value: any) => {
      //单个客户经理
      this.http
        .get(
          "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzcustomemanager/info/"+value
        )
        .subscribe(data => {
          var Data = JSON.parse(data.text()).zzCustomeManager;
          this.items=Data;
            this.JiaoYu=this.items.diplomaInformation.split("|");
            this.RongYu=this.items.honorFor.split("|");
            this.ZhuanYe=this.items.professionalSkills.split("|");
          

          const echarts=scharts as any;
          var myChart = echarts.init(document.getElementById('echart'));
          var option = {
            tooltip: {},
            color:["#4da2ff"],
            radar: {
              radius:"50%",
              nameGap:2,
              splitNumber:3,
              axisLine:{
                lineStyle:{
                  color:"#4da2ff"
                }
              },
              splitLine:{
                lineStyle:{
                  color:"#2fc900",
                  type:"dashed"
                }
              },
              splitArea:{
                show:false
              },
              startAngle:0,
              name: {
                textStyle: {
                  color: '#000'
                }
              },
              indicator: [
                { name: '满意度', max: 10},
                { name: '响应速度', max: 10},
                { name: '业务熟悉度', max: 10},
                { name: '服务亲和力', max: 10},
                { name: '投诉次数', max: 10},
                { name: '专业性', max: 10}
              ]
            },
            series: [{
              type: 'radar',
              data : [
                {
                  value : [
                    this.items.satisfaction,
                    this.items.responseSpeed,
                    this.items.businessFamiliarity,
                    this.items.serviceAffinity,
                    this.items.praise,
                    this.items.speciality
                  ],
                  areaStyle: {
                    normal: {
                      color: 'rgba(93, 160, 246, 0.7)'
                    }
                  }
                }
              ]
            }]
          };
          myChart.setOption(option);
        });
      //客户经理项目经验列表
      this.http
        .get(
          "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzcustomemanagerdetails/info/"+value
        )
        .subscribe(data => {
          let Data = JSON.parse(data.text()).zzCustomeManagerDetails;
          this.itemsshow=Data;
          console.log(this.itemsshow);
        });
      //客户经理月受理单数
      this.http
        .get(
          "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzmonthserviceavg/info/"+value
        )
        .subscribe(data => {
          let Data = JSON.parse(data.text()).zzMonthServiceAvg;



          let Danshu=[];
          for(let i=0;i<Data.length;i++){
            Danshu.push(Data[i].serviceNum);
          }
          console.log(Danshu);


          var zs=0;
          for(let i=0;i<Danshu.length;i++){
            zs+=Danshu[i];
          }
          this.PJDS=(zs/Danshu.length).toFixed(1);

          const echarts=scharts as any;
          var myChart2 = echarts.init(document.getElementById('Biao2'));

          var option2 = {
            color:["#5da0f6","#ea602d","#000"],
            legend: {
              bottom:0,
              data:['去年','今年','平均']
            },
            xAxis: [
              {
                type: 'category',
                data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
                axisPointer: {
                  type: 'shadow'
                }
              }
            ],
            yAxis: [
              {
              },
              {
                show:false,
              }
            ],
            series: [
              // {
              //   name:'去年',
              //   type:'bar',
              //   data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
              // },
              {
                name:'今年',
                type:'bar',
                data:Danshu
              },
              // {
              //   name:'平均',
              //   type:'line',
              //   yAxisIndex: 1,
              //   data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
              // }
            ]
          };

          myChart2.setOption(option2);




        });
      a.unsubscribe();
    });

  }

  initRadarChart() {
    if (this.radar) {
      const chart = new Chart(this.radar.nativeElement, {
        type: "radar",
        data: {
          labels: [
            "业务熟悉度",
            "服务亲和力",
            "好评次数",
            "专业性",
            "满意度",
            "响应速度"
          ],
          datasets: [
            {
              data: [4, 4, 4, 4, 10, 4],
              backgroundColor: "#9cc7fa",
              borderWidth: 0,
              borderColor: "transparent"
            }
          ]
        },
        options: {
          // startAngle: 90,
          legend: {
            display: false
          },
          tooltips: {
            enabled: false
          },
          elements: { point: { radius: 0 } }
        }
      });
      chart.config.options["scale"] = {
        pointLabels: {
          fontSize: 10
        },
        ticks: {
          display: false,
          beginAtZero: true
          // maxTicksLimit: 3
        },
        gridLines: {
          borderDash: [5, 10],
          color: "#4da1ff",
          drawTicks: false
        }
      };
    }
  }

  initBarChart() {
    if (this.bar) {
      new Chart(this.bar.nativeElement, {
        type: "bar",
        data: {
          labels: [
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"
          ],
          datasets: [
            {
              type: "line",
              label: "平均",
              borderColor: "eec000",
              borderWidth: 2,
              fill: false,
              data: [
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5,
                4.5
              ],
              pointRadius: 1
              // legend: false
            },
            {
              type: "bar",
              label: "去年",
              backgroundColor: "#cbe2fa",
              data: [4, 2, 3, 4, 4, 3, 2, 2, 3, 4, 4, 2],
              borderColor: "#6aa7f8",
              borderWidth: 1
            },
            {
              type: "bar",
              label: "今年",
              backgroundColor: "#f7d9df",
              data: [2, 3, 4, 4, 3, 2, 3, 5, 2, 2, 3, 3],
              borderColor: "#ef9eaa",
              borderWidth: 1
            }
          ]
        },
        options: {
          legend: {
            position: "bottom"
          },
          scales: {
            yAxes: [
              {
                display: true,
                ticks: {
                  // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                  // OR //
                  beginAtZero: true // minimum value will be 0.
                }
              }
            ]
          }
        }
      });
    }
  }

  show() {
    this.isShow = true;
  }

  hide() {
    this.close.emit();
  }

  getIcon(index: number) {
    switch (index) {
      case 0:
        return "assets/imgs/教育icon.png";
      case 1:
        return "assets/imgs/项目icon.png";
      case 2:
        return "assets/imgs/荣誉icon.png";
      case 3:
        return "assets/imgs/专业技能icon.png";
      default:
        return "assets/imgs/关闭按钮.png";
    }
  }
}
