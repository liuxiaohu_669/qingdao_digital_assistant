import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { CustomerManagerInfoPage } from './customer-manager-info';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    // CustomerManagerInfoPage,
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(CustomerManagerInfoPage),
    ChartsModule,
    ComponentsModule
  ],
})
export class CustomerManagerInfoPageModule {}
