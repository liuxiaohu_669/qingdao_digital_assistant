import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { BusinessScenarioPage } from './business-scenario';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    BusinessScenarioPage
    // CheckListPage
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(BusinessScenarioPage),
    ComponentsModule
  ]
})
export class BusinessScenarioPageModule {

}
