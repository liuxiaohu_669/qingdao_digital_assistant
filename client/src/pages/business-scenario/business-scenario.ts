import { Component } from "@angular/core";
import { NavController, Events } from "ionic-angular";
import { CostEstimatePage } from "../cost-estimate/cost-estimate";
import { Http } from "@angular/http";
@Component({
  selector: "page-business-scenario",
  templateUrl: "business-scenario.html"
})
export class BusinessScenarioPage {
  Equipment = ["国产柜", "国产柜（合资断路器）"];

  type = [];
  type2 = [];

  typeSelect=1;
  type2Select=1;
  SBid="国产柜";
  JKXid=0;
  GDid=0;
  DLid=0;

  model=[
    ["请选择"],["请选择"],["请选择"]
  ];

  ModelList=[[],[],[]];

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public http: Http
  ) {
    events.subscribe("onClickDiagramButton", item => {});
  }
  ionViewDidLoad() {
    this.http
      .get(
        "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzschemetype/list"
      )
      .subscribe(data => {
        var Data = JSON.parse(data.text()).page.list;
        console.log(Data);
        this.type = Data;
        this.bindJTPZ(Data[0].id);
      });


    this.http
      .get(
        "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzmodelassort/list"
      )
      .subscribe(data => {
        var Data = JSON.parse(data.text()).page.list;
        this.model[0]=[];
        this.model[1]=[];
        this.model[2]=[];
        for(let i=0;i<Data.length;i++){
          switch (Data[i].projectName){
            case "架空线":
              this.model[0].push(Data[i].modelName);
              this.ModelList[0].push(Data[i]);
                  break;
            case "电缆":
              this.model[1].push(Data[i].modelName);
              this.ModelList[1].push(Data[i]);
                  break;
            case "管道":
              this.model[2].push(Data[i].modelName);
              this.ModelList[2].push(Data[i]);
                  break;
          }
        }
      });
  }

  //跳转
  Jump() {
    if(this.SBid===""){
      alert("请选择设备类型");
      return;
    }else if(this.JKXid===0||this.GDid===0||this.DLid===0){
      alert("请选择具配套工程类型");
      return;
    }
    this.navCtrl.push(CostEstimatePage, {
      FALXid:this.typeSelect,
      JTPZid:this.type2Select,
      SBid:this.SBid,
      JKXid:this.JKXid,
      GDid:this.GDid,
      DLid:this.DLid,
    }, { animate: true });
  }

  //设备选择
  equipmentS(e){
    this.SBid=e;
    console.log(this.SBid);
  }
  //架空线选择
  OverheadLinesS(e){
    for(let i=0;i<this.ModelList[0].length;i++){
      if(this.ModelList[0][i].modelName===e){
        console.log(this.ModelList[0][i].id);
        this.JKXid=this.ModelList[0][i].id;
      }
    }
  }
  //电缆选择
  cableS(e){
    for(let i=0;i<this.ModelList[1].length;i++){
      if(this.ModelList[1][i].modelName===e){
        console.log(this.ModelList[1][i].id);
        this.DLid=this.ModelList[1][i].id;
      }
    }
  }
  //管道选择
  PipelineS(e){
    for(let i=0;i<this.ModelList[2].length;i++){
      if(this.ModelList[2][i].modelName===e){
        console.log(this.ModelList[2][i].id);
        this.GDid=this.ModelList[2][i].id;
      }
    }
  }

  bindJTPZ(id){
    this.http
      .get(
        "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzschemetype/info/" +id
      )
      .subscribe(data => {
        var Data = JSON.parse(data.text()).zzSchemeType.zzSchemeTypeConfigurationEntityList;
        console.log(JSON.parse(data.text()));
        this.type2 = Data;
      });
  }

  select(id) {
    this.typeSelect=id;
    console.log(this.typeSelect);
    this.bindJTPZ(id);
  }
  select2(id){
    this.type2Select=id;
    console.log(this.type2Select);
  }
}
