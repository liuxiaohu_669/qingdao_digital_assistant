import { CaseDetailsPageModule } from './../case-details/case-details.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaseShowPage } from './case-show';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CaseShowPage
  ],
  imports: [
    IonicPageModule.forChild(CaseShowPage),
    ComponentsModule,
    CaseDetailsPageModule
  ],
})
export class CaseShowPageModule {}
