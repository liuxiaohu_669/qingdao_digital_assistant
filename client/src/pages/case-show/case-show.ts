import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CaseDetailsPage } from "../case-details/case-details";
import { Http } from "@angular/http";
// @IonicPage()
@Component({
  selector: "page-case-show",
  templateUrl: "case-show.html"
})
export class CaseShowPage {
  List=[
    {
      title:"电能替代",
      list:[]
    },
    {
      title:"分布式电源",
      list:[]
    },
    {
      title:"节能服务",
      list:[]
    }
  ];
  constructor(public navCtrl: NavController,public http:Http) {}

  ionViewDidLoad(){
    this.http.get(
      "http://liuxh1819.vicp.cc/renren-fast/caselibrary/zzcaselibrary/list"
    ).subscribe(data => {
      var Data=JSON.parse(data.text()).page.list;
      console.log(Data);
      for(let i=0;i<Data.length;i++){
        switch (Data[i].typeId){
          case 1:
                this.List[0].list.push(Data[i]);
                break;
          case 2:
                this.List[1].list.push(Data[i]);
                break;
          case 3:
                this.List[2].list.push(Data[i]);
                break;
        }
      }
      console.log(this.List);
    })
  }
  onClickNewCustomerButton(data) {
    this.navCtrl.push(CaseDetailsPage, data, {animate: true})
  }
}
