import { Component } from '@angular/core';
import {IonicPage, NavController, DateTime} from 'ionic-angular';
import { CardDetailsPage } from '../card-details/card-details';
import * as scharts from 'echarts';
import { Http } from "@angular/http";
import { UserInfoService } from "../../services/user";
/**
 * Generated class for the DemandTrackingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-demand-tracking',
  templateUrl: 'demand-tracking.html',
})
export class DemandTrackingPage {
  card=[];
  cadrlength=0;
  constructor(public navCtrl: NavController,public http: Http,public user:UserInfoService) {}


  UserData=this.user.getUser();

  ionViewDidEnter(){
    this.http.get(
      "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirehistory/count/"+this.UserData.custManagerId
    ).subscribe(data=>{
      this.cadrlength=JSON.parse(data.text()).zzRequireHistoryCount;
      // this.cadrlength=Data.length;
    })
  }
  ionViewDidLoad() {


    const echarts=scharts as any;
    var myChart = echarts.init(document.getElementById('echart'));

    this.http.get("http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirecard/info/"+this.UserData.custManagerId).subscribe(data => {
      var Data=JSON.parse(data.text());
      // console.log(new Date().Format("MM/dd"));
      for(let i=0;i<Data.zzRequireCard.length;i++){
        var a=Data.zzRequireCard[i].cardAddTime;
        var b=a.substring(a.indexOf("-")+1,a.indexOf(" "));
        if(b.substr(0,1)==="0"){
          b=b.substr(1,b.length);
        }
        Data.zzRequireCard[i].cardAddTime=b.replace("-","/");
      }
      this.card=Data.zzRequireCard;
      console.log(this.card);
      var gryw=0;
      var grzh=0;
      for(let i=0;i<this.card.length;i++){
        if(this.card[i].requireType==="业务报装"){
          gryw++;
        }else if(this.card[i].requireType==="综合能源"){
          grzh++;
        }
      }


      this.http.get(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzcustomemanager/list"
      ).subscribe(userdata=>{
        var UserLength=JSON.parse(userdata.text()).page.list.length;

        this.http.get(
          "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirecard/count/1"
        ).subscribe(ywdata=>{
          var ywlength=JSON.parse(ywdata.text()).zzRequireCardCount;
          this.http.get(
            "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirecard/count/0"
          ).subscribe(zhdata=>{
            var zhlength=JSON.parse(zhdata.text()).zzRequireCardCount;


            var option = {
              title: {
                text: '项目需求占比分析',
                textStyle:{
                  color:"#FFF"
                }
              },
              color:["#FFE154","#00b4ff"],
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['团队平均', '本人跟踪'],
                show:true,
                left:"10%",
                bottom:"0",
                textStyle:{
                  color:"#FFF"
                }
              },
              grid: {
                left: '3%',
                right: '4%',
                top: '25%',
                height:"50%",
                containLabel: true
              },
              xAxis: {
                type: 'value',
                boundaryGap: [0, 0.01]
              },
              yAxis: {
                type: 'category',
                data: ['综合能源','业务报装'],
                textStyle:{
                  color:"#FFF"
                }
              },
              series: [
                {
                  name: '团队平均',
                  type: 'bar',
                  data: [(ywlength/UserLength), (zhlength/UserLength)]
                },
                {
                  name: '本人跟踪',
                  type: 'bar',
                  data: [gryw, grzh]
                }
              ]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);


          })
        })
      })

 










    })






  }



  onClickNewCustomerButton(id) {
    if(id==-1){
      this.navCtrl.push(CardDetailsPage, -1, {animate: true})
    }else{
      console.log(this.card);
      this.navCtrl.push(CardDetailsPage, this.card[id], {animate: true})
    }
  }
}
