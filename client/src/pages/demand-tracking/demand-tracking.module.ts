import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemandTrackingPage } from './demand-tracking';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DemandTrackingPage,
  ],
  imports: [
    IonicPageModule.forChild(DemandTrackingPage),
    ComponentsModule
  ],
})
export class DemandTrackingPageModule {}
