import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CaseDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-case-details',
  templateUrl: 'case-details.html',
})
export class CaseDetailsPage {
  pageData=this.navParams.data;
  video=document.getElementById("video");
  playtype=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log(this.pageData);
    console.log('ionViewDidLoad CaseDetailsPage');
  }
  pause(){
    this.playtype=false;
  }
  play(){
    var video=document.getElementById("video")
    this.playtype=true;
    video.play();
  }
}
