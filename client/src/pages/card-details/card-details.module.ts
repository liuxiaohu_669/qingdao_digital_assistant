import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { CardDetailsPage } from './card-details';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CardDetailsPage
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(CardDetailsPage),
    ComponentsModule
  ],
})
export class CardDetailsPageModule {}
