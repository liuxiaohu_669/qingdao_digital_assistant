import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";
import { DemandTrackingPage } from '../demand-tracking/demand-tracking';
import { UserInfoService } from "../../services/user";
/**
 * Generated class for the CardDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-card-details',
  templateUrl: 'card-details.html',
})
export class CardDetailsPage {
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http,public user:UserInfoService) {
  }
  type = ["业务报装", "综合能源"];
  stage = ["初步咨询" , "方案草拟" , "现场勘查" , "方案认可"];
  EditStatus=false;
  CData=this.navParams.data==-1?
  {
    "contacts": "",
    "custManagerId": 0,
    "custName": "",
    "custNumber": "",
    "phone": "",
    "projectName": "",
    "requireId": 0,
    "requireName": "",
    "requirePhase": "",
    "requireType": "",
    "requireTypeId": 0
  }:
  this.navParams.data;
  editText="";
  cardShow=[];
  IsAdd=false;
  thisEditid=0;
  UserData=this.user.getUser();
  XQLX="";
  XQJD="";
  index1=(()=>{
    if(this.CData.requireType==="综合能源"){
      return 1;
    }else{
      return 0;
    }
  })();
  index2= (()=>{
    switch (this.CData.requirePhase){
      case "初步咨询":
        return 0;
        break;
      case "方案草拟":
        return 1;
        break;
      case "现场勘查":
        return 2;
        break;
      case "方案认可":
        return 3;
        break;
    }
    return 0;
  })();
  ionViewDidLoad(){
    this.updateList();
  }

  updateList(){
    console.log(this.CData);
    if(this.navParams.data==-1){
      var moren=document.getElementById("moren");
      moren.focus();
    }else{

      console.log(this.index2);

      this.http.get(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirehistory/info/"+this.CData.requireId
      ).subscribe(data =>{
        var Data=JSON.parse(data.text()).zzRequireHistory;

        for(let i=0;i<Data.length;i++){
          var a=Data[i].requireDate;
          var b=a.substring(a.indexOf("-")+1,a.indexOf(" "));
          if(b.substr(0,1)==="0"){
            b=b.substr(1,b.length);
          }
          Data[i].requireDate=b.replace("-","/");
        }
        this.cardShow=Data;
      })
    }
  }

  switchCapacity(index: number) {
    this.XQLX=this.type[index];
  }
  switchCapacity1(index: number) {
    this.XQJD=this.stage[index];
  }


  //新增跟踪卡片
  SaveCase(){

    var requireTypeId=0;
    if(this.XQLX==="业务报装"){
      requireTypeId=1;
    }else{
      requireTypeId=0;
    }
    console.log(this.XQLX);

    //为0为新增

    console.log(this.CData);
    if(this.navParams.data==-1){
      this.http.post(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirecard/save",
        {
          "contacts": this.CData.contacts,
          "custManagerId": this.UserData.custManagerId,
          "custName": this.CData.custName,
          "custNumber": this.CData.custNumber,
          "phone": this.CData.phone,
          "projectName": this.CData.requireName,
          "requirePhase": this.XQJD,
          "requireName": this.CData.requireName,
          "requireType": this.XQLX,
          "requireTypeId": requireTypeId
        }
      ).subscribe(data=>{
        this.navCtrl.push(DemandTrackingPage, {}, {animate: true})
      })
    }else{
      //为修改
      this.http.post(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirecard/update",
        {
          "contacts": this.CData.contacts,
          "custManagerId": this.UserData.custManagerId,
          "custName": this.CData.custName,
          "custNumber": this.CData.custNumber,
          "phone": this.CData.phone,
          "projectName": this.CData.requireName,
          "requirePhase": this.XQJD,
          "requireName": this.CData.requireName,
          "requireType": this.XQLX,
          "requireTypeId": requireTypeId,
          "requireId": this.CData.requireId
        }
      ).subscribe(data=>{
        this.navCtrl.push(DemandTrackingPage, {}, {animate: true})
      })
    }

  }


  //编辑跟踪历史
  editClick(id){
    this.IsAdd=false;
    this.editText=id.requireContent;
    this.thisEditid=id.id;
    this.EditStatus=true;
  }

  //新增跟踪历史
  addclick(){
    if(this.CData==-1){
      alert("请先保存跟踪历史");
      return;
    }
    this.editText="";
    this.IsAdd=true;
    this.EditStatus=true;
  }

  //跟踪历史返回
  returnclick(){
    this.EditStatus=false;
  }

  //跟踪历史保存按钮
  textsave(){
    if(this.IsAdd){
      if(this.editText===""){
        alert("请输入详细信息！");
        return;
      }
      //新增
      this.http.post(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirehistory/save",
        {
          "custManagerId":this.UserData.custManagerId,
          "requireContent": this.editText,
          "requireId": this.CData.requireId
        }
      ).subscribe(data =>{
        this.updateList();
        this.EditStatus=false;
      })
    }else{
      //修改
      this.http.post(
        "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirehistory/update",
        {
          "id":this.thisEditid,
          "requireContent": this.editText,
          "requireId": this.CData.requireId
        }
      ).subscribe(data =>{
        console.log(data);
        this.updateList();
        this.EditStatus=false;
      })
    }

  }


  rem2oveClick(id){
    this.http.post(
    "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzrequirehistory/delete",
      [id]
  ).subscribe(data =>{
    var Data=this.cardShow;
      console.log(Data);
      this.updateList();
    })
  }
}
