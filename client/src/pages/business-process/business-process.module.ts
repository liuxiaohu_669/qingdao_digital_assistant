import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { BusinessProcessPage } from './business-process';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    BusinessProcessPage
    // CheckListPage
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(BusinessProcessPage),
    ComponentsModule
  ],
  exports: [
  ]
})
export class BusinessProcessPageModule {

}
