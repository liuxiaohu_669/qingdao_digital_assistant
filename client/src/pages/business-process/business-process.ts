import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { CheckListPage } from '../check-list/check-list';

@Component({
  selector: "page-business-process",
  templateUrl: "business-process.html"
})
export class BusinessProcessPage {
  id=0;
  myList = [
    {
      title: "步骤1：用电申请",
      imgurl:"assets/imgs/用电申请.png",
      content:
        "　　请您按照材料提供要求准备申请资料，详见申请资料清单。\n　　若您暂时无法提供全部资料，我们将提供“一证受理”服务。在您签署《承诺书》后，我们将先行受理，启动后续工作。",
      children: ["查看承诺书", "查看申请资料清单"]
    },
    {
      title: "步骤2：确定方案",
      imgurl:"assets/imgs/确定方案.png",
      content:
        "　　在受理您用电申请后，我们将安排客户经理按照与您约定的时间到现场查看供电条件，并在15个工作日（双电源客户30个工作日）内答复供电方案。根据国家《供电营业规则》规定，产权分界点以下部分由您负责施工，产权分界点以上工程由供电企业负责。"
    },
    {
      title: "步骤3：工程设计",
      imgurl:"assets/imgs/工程设计.png",
      content:
        "　　请您自主选择有相应资质的设计单位开展受电工程设计。\n　　对于重要或特殊负荷客户，设计完成后，请及时提交设计文件，我们将在10个工作日内完成审查；其他客户仅查验设计单位资质文件"
    },
    {
      title: "步骤4：工程施工",
      imgurl:"assets/imgs/工程施工.png",
      content:
        "　　请您自主选择有相应资质的施工单位开展受电工程施工。\n　　对于重要或特殊负荷客户，在电缆管沟、接地网等隐蔽工程覆盖前，请及时通知我们进行中间检查，我们将于3个工作日内完成中间检查。\n　　工程竣工后，请及时报验，我们将于5个工作日内完成竣工检验。"
    },
    {
      title: "步骤5：装表装电",
      imgurl:"assets/imgs/装电接表.png",
      content:
        "　　在竣工检验合格，签订《供用电合同》及相关协议，并按照政府物价部门批准的收费标准结清业务费用后，我们将在5个工作日内为您装表接电。"
    }
  ];

  businessTypeList = ["新装、增容"];
  isBlur = false;
  isShowCheckListPage = false;

  xiabutton=true;
  shangbutton=false;

  constructor(public navCtrl: NavController) {}

  onClickButton(index: number) {
    if (index === 0) {
    } else if (index === 1) {
      this.isShowCheckListPage = true;
      this.isBlur = true;
    }
  }

  closeCheckListPage() {
    this.navCtrl.push(CheckListPage, {}, {animate: true});
  }


  bz(i){
    this.id=i;
    this.buttonShow(this.id);
  }
  
  PreviousStepClick(){
    if(this.id>0){
      this.id--;
    }
    this.buttonShow(this.id);
  }

  nextStepClick(){
    if(this.id<4){
      this.id++;
    }
    this.buttonShow(this.id);
  }

  buttonShow(id){
    if(id===0){
        this.xiabutton=true;
        this.shangbutton=false;
    }
    else if(id>0&&id<4){
      this.xiabutton=true;
      this.shangbutton=true;
    }else{
      this.xiabutton=false;
      this.shangbutton=true;
    }
  }

}
