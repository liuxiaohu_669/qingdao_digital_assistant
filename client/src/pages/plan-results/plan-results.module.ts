import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ComponentsModule} from "../../components/components.module";
import { PlanResultsPage } from './plan-results';
import { NgxQRCodeModule } from 'ngx-qrcode2';
@NgModule({
  declarations: [
    PlanResultsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanResultsPage),
    ComponentsModule,
    NgxQRCodeModule
  ],
})
export class PlanResultsPageModule {}
