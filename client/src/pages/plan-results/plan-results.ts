import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";
import * as $ from 'jquery';
/**
 * Generated class for the PlanResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-plan-results',
  templateUrl: 'plan-results.html',
})
export class PlanResultsPage {
  id=this.navParams.data;
  Data={};
  createdCode="";
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http) {
  } 

  ionViewDidEnter() {
    this.createdCode="";
    this.createdCode="http://liuxh1819.vicp.cc/renren-fast/static/Program.html?id="+this.id.xid+"&bh="+this.id.id;
    console.log(this.createdCode);
    this.http.get(
      "http://liuxh1819.vicp.cc/renren-fast/schemegeneration/zzgeneratescheme/info/"+this.id.id
    ).subscribe(data=>{
      this.Data=JSON.parse(data.text()).zzGenerateScheme;
      console.log(this.Data);
    })
    console.log('ionViewDidLoad PlanResultsPage');
  }

}
