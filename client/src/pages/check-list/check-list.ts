import { Component, EventEmitter, Output } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the CheckListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-check-list",
  templateUrl: "check-list.html"
})
export class CheckListPage {
  @Output() onClickClose = new EventEmitter();

  checkList = [
    {
      requirement:
        "用电主体资格证明材料（如身份证、营业执照、组织机构代码证等）",
      tips:
        "申请时必备。已提供加载统一社会信用代码的营业执照的，不再要求提供组织机构代码和税务登记证明。"
    },
    {
      requirement:
        "客户承诺书（如果客户申请时提供了所有齐全资料的，可不要求签署该承诺书。）",
      tips:
        "如果暂不能提供与用电人身份一致的有效产权证明原件及复印件的，签署承诺书后可在后续环节补充。"
    },
    {
      requirement: "产权证明（复印件）或其它证明文书",
      tips:
        "如果暂不能提供与用电人身份一致的有效产权证明原件及复印件的，签署承诺书后可在后续环节补充。"
    },
    {
      requirement:
        "企业、工商、事业单位、社会团体的申请用电委托代理人办理时，应提供：<br>（1）授权委托书或单位介绍信（原件）；<br>（2）经办人有效身份证明复印件（包括身份证、军人证、护照、户口簿或公安机关户籍证明等）。",
      tips: "非企业负责人（法人代表）办理时必备"
    },
    {
      requirement: "政府职能部门有关本项目立项的批复、核准、备案文件",
      tips: "高危及重要客户、高耗能客户必备"
    },
    {
      requirement:
        "高危及重要客户：<br>（1）保安负荷具体设备和明细<br>（2）非电性质安全措施相关资料；<br>（3）应急电源（包括自备发电机组）相关资料。",
      tips: "高危及重要客户必备"
    },
    {
      requirement:
        "煤矿客户需增加以下资料:<br>（1）采矿许可证；<br>（2）安全生产许可证。",
      tips: "煤矿客户必备"
    },
    {
      requirement:
        "非煤矿山客户需增加以下资料:<br>（1）采矿许可证；<br>（2）安全生产许可证；<br>（3）政府主管部门批准文件。",
      tips: "非煤矿山客户必备"
    },
    {
      requirement: "税务登记证复印件",
      tips:
        "根据客户用电主体类别提供。已提供加载统一社会信用代码的营业执照的，不再要求提供税务登记证明。"
    },
    {
      requirement: "一般纳税人资格复印件",
      tips: "需要开具增值税发票的客户必备"
    },
    {
      requirement:
        "对涉及国家优待电价的应提供政府有权部门核发的资质证明和工艺流程",
      tips: "享受国家优待电价的客户必备"
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad CheckListPage");
  }

  onClickCloseButton() {
    this.onClickClose.emit();
  }
}
