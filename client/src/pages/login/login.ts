import { Component, ViewChild } from "@angular/core";
import { NavController, Nav } from "ionic-angular";
import { HomePage } from "../home/home";
import { Http } from "@angular/http";
import { UserInfoService } from "../../services/user";
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  @ViewChild(Nav) nav: Nav;
  constructor(public navCtrl: NavController,public http:Http,public user:UserInfoService) {
  }
  UserName="";
  password="";
  XX="";
  onClickLogin(): void {
    this.XX="登陆中...请稍后";
    if(this.UserName==""||this.password==""){
      this.XX="手机号和密码不能为空";
      return
    }

    this.http.post("http://liuxh1819.vicp.cc/renren-fast/app/login",{
      "mobile":this.UserName,
      "password":this.password
    }).subscribe(data => {
      var Data=JSON.parse(data.text());
      if(Data.msg==="success"){
        this.http.get(
          "http://liuxh1819.vicp.cc/renren-fast/customemanager/zzcustomemanager/info/"+Data.user.customemanagerEntity
        ).subscribe(data=>{
          var UserData=JSON.parse(data.text()).zzCustomeManager;
          this.user.setUserimg(Data.user.imgPath);
          this.user.setUser(UserData);
          this.navCtrl.setRoot(HomePage);
        })
      }else {
        this.XX=Data.msg;
      }
    })
  }
}
