import { Injectable, EventEmitter, OnInit } from '@angular/core';
import { Http } from "@angular/http";
@Injectable()
export class UserInfoService implements OnInit {
  public eventEmit: any;
  constructor(public http:Http) {
  }

  UserModel={};
  Userimg="";
  
  getUser(){
    return this.UserModel;
  }
  setUser(userModel){
   this.UserModel=userModel;
  }
  
  getUserimg(){
    return this.Userimg;
  }
  setUserimg(userModelimg){
   this.Userimg=userModelimg;
  }


  ngOnInit() {

  }
}
