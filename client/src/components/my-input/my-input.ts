import { Component, Input, Renderer2, ViewChild } from '@angular/core';

/**
 * Generated class for the MyInputComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

 @Component({
  selector: 'my-input',
  templateUrl: 'my-input.html'
})
export class MyInputComponent {

  @ViewChild('myInput') myInput;
  @Input() title: string
  @Input() inputText: string
  @Input() white: string

  constructor(private renderer: Renderer2) {}

  ngAfterViewInit() {
    console.log(this.white)
    if (this.white === 'true') {
      this.renderer.setStyle(
        this.myInput.nativeElement,
        'background-color',
        'white'
      );
      this.renderer.setStyle(
        this.myInput.nativeElement,
        'border',
        '0.5px solid lightgray'
      );
    }
  }

}
