import { Component } from '@angular/core';

/**
 * Generated class for the PaginationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pagination',
  inputs: ['totalItems: total-items'],
  templateUrl: 'pagination.html'
})
export class PaginationComponent {

  totalItems: string
  pageNumber = []

  constructor() {
    console.log('Hello PaginationComponent Component');
    // this.text = this.totalItems;
    const total = parseInt(this.totalItems);
    for (let index = 0; index < total; index++) {
      this.pageNumber.push(index)
      
    }
  }
}
