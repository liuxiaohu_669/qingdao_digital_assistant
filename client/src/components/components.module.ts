import { MyHeaderComponent } from './my-header/my-header';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";

import { CustomerManagerCardComponent } from "./customer-manager-card/customer-manager-card";
import { PaginationComponent } from "./pagination/pagination";
import { CommonModule } from "@angular/common";
import { DropListComponent } from "./drop-list/drop-list";
import { GlassBgComponent } from "./glass-bg/glass-bg";
import { CaseCoverComponent } from './case-cover/case-cover';
import { MyInputComponent } from './my-input/my-input';
import { CircuitDiagramComponent } from './circuit-diagram/circuit-diagram';
import { DropList2Component } from './drop-list2/drop-list2';

@NgModule({
  declarations: [
    CustomerManagerCardComponent,
    PaginationComponent,
    DropListComponent,
    GlassBgComponent,
    CaseCoverComponent,
    MyInputComponent,
    CircuitDiagramComponent,
    DropList2Component,
    MyHeaderComponent
  ],
  imports: [IonicModule, CommonModule],
  exports: [
    CustomerManagerCardComponent,
    PaginationComponent,
    DropListComponent,
    GlassBgComponent,
    CaseCoverComponent,
    MyInputComponent,
    CircuitDiagramComponent,
    DropList2Component,
    MyHeaderComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
