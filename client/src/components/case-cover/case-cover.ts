import { Component, Input } from '@angular/core';

/**
 * Generated class for the CaseCoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'case-cover',
  templateUrl: 'case-cover.html'
})
export class CaseCoverComponent {

  @Input() img: string

  constructor() {

  }

}
