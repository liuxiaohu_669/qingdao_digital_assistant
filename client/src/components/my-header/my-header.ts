import { Component,Output,EventEmitter } from '@angular/core';
import { UserInfoService } from "../../services/user";
@Component({
  selector: 'my-header',
  templateUrl: 'my-header.html'
})
export class MyHeaderComponent {
  @Output()
  changeNumber: EventEmitter<number> = new EventEmitter();
  text: string;
  @Output() close = new EventEmitter();
  constructor(public user:UserInfoService) {
  }
  Data=this.user.getUser();
  imgurl=this.user.getUserimg();
  ngOnInit(){
    console.log(this.imgurl);
  }
}
