import { Component, Input } from "@angular/core";
import { Events } from "ionic-angular";

/**
 * Generated class for the CircuitDiagramComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "circuit-diagram",
  templateUrl: "circuit-diagram.html"
})
export class CircuitDiagramComponent {
  @Input() type: string;
  @Input() capacity: string;

  diagramImage: string;

  diagramButtons = {
    杆上变: {
      计量出线一体柜: ["国产柜", "国产柜(合资断路器)"],
      架空线: [
        "JKLV-10-70(160元/米)",
        "JKLV-10-150(250元/米)",
        "JKLV-10-185(280元/米)",
        "JKLV-10-240(290元/米)"
      ],
      电缆线: [
        "YJV22-8.7/15-3*70(250元/米)",
        "YJV22-8.7/15-3*95(320元/米)",
        "YJV22-8.7/15-3*150(460元/米)",
        "YJV22-8.7/15-3*300(830元/米)"
      ]
    },
    室内干式变: {
      进线柜: ["国产柜", "国产柜(合资断路器)"],
      变压器柜: ["国产柜", "国产柜(合资断路器)"],
      电缆线: [
        "YJV22-8.7/15-3*70(250元/米)",
        "YJV22-8.7/15-3*95(320元/米)",
        "YJV22-8.7/15-3*150(460元/米)",
        "YJV22-8.7/15-3*300(830元/米)"
      ],
      电容柜: ["国产柜", "国产柜(合资断路器)"],
      馈电柜: ["国产柜", "国产柜(合资断路器)"],
      计量兼总柜: ["国产柜", "国产柜(合资断路器)"]
    },
    室内油浸变: {
      电缆线: [
        "YJV22-8.7/15-3*70(250元/米)",
        "YJV22-8.7/15-3*95(320元/米)",
        "YJV22-8.7/15-3*150(460元/米)",
        "YJV22-8.7/15-3*300(830元/米)"
      ],
      电容柜: ["国产柜", "国产柜(合资断路器)"],
      馈电柜: ["国产柜", "国产柜(合资断路器)"],
      计量兼总柜: ["国产柜", "国产柜(合资断路器)"]
    }
  };

  constructor(public events: Events) {
    
  }

  ngOnChanges() {
    console.log(this.type);
    if (this.type === "杆上变") {
      switch (this.capacity) {
        case "50KVA":
          this.diagramImage = "assets/imgs/线路图/杆上变-50.png";
          break;
        case "80KVA":
          this.diagramImage = "assets/imgs/线路图/杆上变-50.png";
          break;
        default:
          this.diagramImage = "assets/imgs/线路图/杆上变-50.png";
          break;
      }
    } else if (this.type === "室内干式变") {
      switch (this.capacity) {
        case "100KVA":
          this.diagramImage = "assets/imgs/线路图/室内干式变-100.png";
          break;
        case "125KVA":
          this.diagramImage = "assets/imgs/线路图/室内干式变-125.png";
          break;
        case "160KVA":
          this.diagramImage = "assets/imgs/线路图/室内干式变-160.png";
          break;
        default:
          this.diagramImage = "assets/imgs/线路图/室内干式变-100.png";
          break;
      }
    } else if (this.type === "室内油浸变") {
      switch (this.capacity) {
        case "100KVA":
          this.diagramImage = "assets/imgs/线路图/室内油浸变-100.png";
          break;
        case "125KVA":
          this.diagramImage = "assets/imgs/线路图/室内油浸变-125.png";
          break;
        case "160KVA":
          this.diagramImage = "assets/imgs/线路图/室内油浸变-160.png";
          break;
        default:
          this.diagramImage = "assets/imgs/线路图/室内油浸变-160.png";
          break;
      }
    }
  }

  getButtonItemList(buttonName: string) {
    return this.diagramButtons[this.type][buttonName];
  }

  onClickItem(obj: {index: number, title: string, item: string}) {
    this.events.publish("onClickDiagramButton", this.diagramButtons[this.type][obj.title][obj.index]);
  }
}
