import { Component, Input, Output, EventEmitter } from "@angular/core";

/**
 * Generated class for the DropList2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "drop-list2",
  templateUrl: "drop-list2.html"
})
export class DropList2Component {
  @Input() list: string[] = [];
  @Input() title: string = "";
  @Output() clickItem = new EventEmitter();

  isShowList = false;

  constructor() {}

  onClickButton() {
    if (this.list.length > 1) {
      this.isShowList = !this.isShowList;
    }
  }

  onClickListItem(index: number) {
    this.isShowList = false;
    // this.selectedItem = this.list[index];
    this.clickItem.emit({
      index: index,
      title: this.title,
      item: this.list[index]
    });
  }
}
