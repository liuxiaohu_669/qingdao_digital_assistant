import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "drop-list",
  templateUrl: "drop-list.html"
})
export class DropListComponent {
  @Input() list: string[] = [];
  @Input() color: string;
  @Input() selectedIndex:string;
  @Input('font-size') fontSize: string;
  @Output() clickItem = new EventEmitter();

  selectedItem: string;
  selectedItemStyle: object;
  itemStyle: object;
  isShowList = false;

  constructor() {}

  ngAfterViewInit() {
    if (this.list.length > 0) {
      this.selectedItem = this.list[this.selectedIndex];
      // this.selectedItem = this.list[0];
    }
    this.selectedItemStyle = {
      color: this.color,
      "font-size": this.fontSize
    };
    // this.itemStyle = {
    //   color: this.color,
    //   "font-size": this.fontSize
    // };
  }

  onClickButton() {
    if (this.list.length > 1) {
      this.isShowList = !this.isShowList;
    }
  }

  onClickListItem(index: number) {
    this.isShowList = false;
    this.selectedItem = this.list[index];
    this.clickItem.emit(index)
  }
}
