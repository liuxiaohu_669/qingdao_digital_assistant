import { Component, Output, EventEmitter, Input } from "@angular/core";

/**
 * Generated class for the CustomerManagerCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "customer-manager-card",
  templateUrl: "customer-manager-card.html",
  inputs: ["clickFuction"]
})
export class CustomerManagerCardComponent {
  Url="http://liuxh1819.vicp.cc/renren-fast";
  @Input() info: any;
  @Output() buttonClick = new EventEmitter();

  constructor() {
  }

  onClickShowInfo() {
    this.buttonClick.emit();
  }
}
