const fs = require('fs');
const Koa = require('koa');
const serve = require('koa-static');
const staticCache = require('koa-static-cache')

const app = new Koa();

app.use(staticCache('../client/platforms/browser/www'), {
  maxAge: 365 * 24 * 60 * 60
})

app.use(serve('../client/platforms/browser/www'));
// response
app.use(ctx => {
  ctx.response.type = 'html';
  ctx.response.body = fs.createReadStream('../client/platforms/browser/www/index.html');
});

app.listen(80);